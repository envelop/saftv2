// SPDX-License-Identifier: MIT
// ENVELOP(NIFTSY) protocol V1.   SAFT  with Blast Points

import "./TrustedWrapperV2.sol";
import "./BlastPoints.sol";

pragma solidity 0.8.21;

contract TrustedWrapperV2BlastPoints is TrustedWrapperV2, BlastPoints {


    constructor ( address _erc20, address _trusted,address _pointsOperator)
        TrustedWrapperV2(_erc20, _trusted) 
        BlastPoints(_pointsOperator)
    {

    } 

}