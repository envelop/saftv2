// SPDX-License-Identifier: MIT
// ENVELOP(NIFTSY) protocol V1 for NFT. Batch Worker for Blast

import "./BatchWorkerV2.sol";
import "./BlastPoints.sol";



pragma solidity 0.8.21;

contract BatchWorkerV2Blast is  BatchWorkerV2, BlastPoints {
    constructor (address _subscrRegistry, address _pointsOperator)
        BatchWorkerV2(_subscrRegistry) 
        BlastPoints(_pointsOperator)
    {

    } 
    
}