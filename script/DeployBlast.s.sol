// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {BatchWorkerV2Blast} from "../contracts/BatchWorkerV2Blast.sol";
import {TrustedWrapperV2BlastPoints} from "../contracts/TrustedWrapperV2BlastPoints.sol";
import {TechTokenV1} from "@envelop-protocol-v1/contracts/TechTokenV1.sol";
import {EnvelopwNFT721} from "@envelop-protocol-v1/contracts/EnvelopwNFT721.sol";
import {EnvelopwNFT1155} from "@envelop-protocol-v1/contracts/EnvelopwNFT1155.sol";
import "@envelop-protocol-v1/contracts/LibEnvelopTypes.sol";



contract DeployScriptBlast is Script {
    using stdJson for string;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, %s", msg.sender, msg.sender.balance);

        // Load json with chain params
        string memory root = vm.projectRoot();
        string memory params_path = string.concat(root, "/script/chain_params.json");
        string memory params_json_file = vm.readFile(params_path);
        string memory key;

        address pointsOperator = 0x303cD2A927D9Cb6F5CE03b88a4e3E2528baEDF40;

        // Define constructor params
        address sub_reg;   
        key = string.concat(".", vm.toString(block.chainid),".sub_reg");
        if (vm.keyExists(params_json_file, key)) 
        {
            sub_reg = params_json_file.readAddress(key);
        } else {
            sub_reg = address(0);
        }
        console2.log("sub_reg: %s", sub_reg); 
        
        
        
        //////////   Deploy   //////////////
        vm.startBroadcast();
        TechTokenV1 techToken = new TechTokenV1();
        BatchWorkerV2Blast saftv2 = new BatchWorkerV2Blast(sub_reg, pointsOperator);
        TrustedWrapperV2BlastPoints wrapperTrustedV2 = new TrustedWrapperV2BlastPoints(
            address(techToken), 
            address(saftv2),
            pointsOperator
        );
        EnvelopwNFT721 wnft721   = new EnvelopwNFT721(
            'ENVELOP 721 swNFT Collection SAFT', 
            'wNFT', 
            'https://api.envelop.is/metadata/',
            address(wrapperTrustedV2)
        );
        EnvelopwNFT1155 wnft1155   = new EnvelopwNFT1155(
            'ENVELOP 1155 swNFT Collection SAFT', 
            'wNFT', 
            'https://api.envelop.is/metadata/',
            address(wrapperTrustedV2)
        );
        vm.stopBroadcast();
        
        ///////// Pretty printing ////////////////
        
        string memory path = string.concat(root, "/script/explorers.json");
        string memory json = vm.readFile(path);
        console2.log("Chain id: %s", vm.toString(block.chainid));
        string memory explorer_url = json.readString(
            string.concat(".", vm.toString(block.chainid))
        );
        
        console2.log("\n**BatchWorkerV2Blast**  ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(saftv2));
        console2.log("\n**TrustedWrapperV2BlastPoints** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(wrapperTrustedV2));
        console2.log("\n**TechTokenV1** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(techToken));
        console2.log("\n**EnvelopwNFT721** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(wnft721));
        console2.log("\n**EnvelopwNFT1155** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(wnft1155));

        console2.log("```python");
        console2.log("saftv2 = BatchWorkerV2Blast.at('%s')", address(saftv2));
        console2.log("wrapperTrustedV2 = TrustedWrapperV2BlastPoints.at('%s')", address(wrapperTrustedV2));
        console2.log("techToken = TechTokenV1.at('%s')", address(techToken));
        console2.log("wnft721 = EnvelopwNFT721.at('%s')", address(wnft721));
        console2.log("wnft1155 = EnvelopwNFT1155.at('%s')", address(wnft1155));
        console2.log("```");
   
        ///////// End of pretty printing ////////////////

        ///  Init ///
        console2.log("Init transactions....");
        vm.startBroadcast();

        wrapperTrustedV2.setTrustedOperatorStatus(sub_reg, true);
        wrapperTrustedV2.setTrustedOperatorStatus(address(saftv2), true);
        saftv2.setTrustedWrapper(address(wrapperTrustedV2));
        saftv2.setSubscriptionOnOff(false);
        wrapperTrustedV2.setWNFTId(ETypes.AssetType.ERC721, address(wnft721), 1);
        wrapperTrustedV2.setWNFTId(ETypes.AssetType.ERC1155, address(wnft1155),1);
                
        vm.stopBroadcast();
        console2.log("Initialisation finished");

    }
}
