// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {BatchWorkerV2} from "../contracts/BatchWorkerV2.sol";
import {TrustedWrapperV2} from "../contracts/TrustedWrapperV2.sol";
import {TechTokenV1} from "@envelop-protocol-v1/contracts/TechTokenV1.sol";


contract DeployScript is Script {
    using stdJson for string;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, %s", msg.sender, msg.sender.balance);

        // Load json with chain params
        string memory root = vm.projectRoot();
        string memory params_path = string.concat(root, "/script/chain_params.json");
        string memory params_json_file = vm.readFile(params_path);
        string memory key;

        // Define constructor params
        address sub_reg;   
        key = string.concat(".", vm.toString(block.chainid),".sub_reg");
        if (vm.keyExists(params_json_file, key)) 
        {
            sub_reg = params_json_file.readAddress(key);
        } else {
            sub_reg = address(0);
        }
        console2.log("sub_reg: %s", sub_reg); 
        
        
        
        //////////   Deploy   //////////////
        vm.startBroadcast();
        TechTokenV1 techToken = new TechTokenV1();
        BatchWorkerV2 saftv2 = new BatchWorkerV2(sub_reg);
        TrustedWrapperV2 wrapperTrustedV2 = new TrustedWrapperV2(address(techToken), address(saftv2));
        vm.stopBroadcast();
        
        ///////// Pretty printing ////////////////
        
        string memory path = string.concat(root, "/script/explorers.json");
        string memory json = vm.readFile(path);
        console2.log("Chain id: %s", vm.toString(block.chainid));
        string memory explorer_url = json.readString(
            string.concat(".", vm.toString(block.chainid))
        );
        
        console2.log("\n**BatchWorkerV2**  ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(saftv2));
        console2.log("\n**TrustedWrapperV2** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(wrapperTrustedV2));

        console2.log("```python");
        console2.log("saftv2 = BatchWorkerV2.at('%s')", address(saftv2));
        console2.log("wrapperTrustedV2 = TrustedWrapperV2.at('%s')", address(wrapperTrustedV2));
        console2.log("```");
   
        ///////// End of pretty printing ////////////////

        ///  Init ///
        console2.log("Init transactions....");
        vm.startBroadcast();

        wrapperTrustedV2.setTrustedOperatorStatus(sub_reg, true);
        wrapperTrustedV2.setTrustedOperatorStatus(address(saftv2), true);
        saftv2.setTrustedWrapper(address(wrapperTrustedV2));
        saftv2.setSubscriptionOnOff(false);
                
        vm.stopBroadcast();
        console2.log("Initialisation finished");

    }
}
