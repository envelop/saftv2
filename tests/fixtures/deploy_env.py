import pytest
#from brownie import chain
zero_address = '0x0000000000000000000000000000000000000000'
############ Mocks ########################
@pytest.fixture(scope="module")
def dai(accounts, TokenMock):
    dai = accounts[0].deploy(TokenMock,"DAI MOCK Token", "DAI")
    yield dai

@pytest.fixture(scope="module")
def weth(accounts, TokenMock):
    w = accounts[0].deploy(TokenMock,"WETH MOCK Token", "WETH")
    yield w

@pytest.fixture(scope="module")
def niftsy20(accounts, TokenMock):
    w = accounts[0].deploy(TokenMock,"Niftsy Token", "NIFTSY")
    yield w    

@pytest.fixture(scope="module")
def erc721mock(accounts, Token721Mock): 
    """
    NFT 721 with URI
    """
    t = accounts[0].deploy(Token721Mock, "Simple NFT with URI", "XXX")
    t.setURI(0, 'https://maxsiz.github.io/')
    yield t     

@pytest.fixture(scope="module")
def sub_reg(pm, accounts):
    sr = pm('dao-envelop/subscription@0.0.3').SubscriptionRegistry
    yield sr.deploy(accounts[0], {'from':  accounts[0]})

@pytest.fixture(scope="module")
def sub_reg1(pm, accounts):
    sr = pm('dao-envelop/subscription@0.0.3').SubscriptionRegistry
    yield sr.deploy(accounts[0], {'from':  accounts[0]})

@pytest.fixture(scope="module")
def agent(pm, accounts):
    ag = pm('dao-envelop/subscription@0.0.3').EnvelopAgentWithRegistry
    yield ag.deploy({'from':  accounts[0]})

@pytest.fixture(scope="module")
def saftv2(accounts, sub_reg, BatchWorkerV2):
    s = accounts[0].deploy(BatchWorkerV2, sub_reg)
    yield s

@pytest.fixture(scope="module")
def wnft721(pm, accounts):
    wnft = pm('dao-envelop/envelop-protocol-v1@1.1.0').EnvelopwNFT721
    yield wnft.deploy(
        "Envelop wNFT", 
        "wNFT", 
        "https://api.envelop.is/metadata/", 
    {'from':  accounts[0]})

@pytest.fixture(scope="module")
def techERC20ForSaftV2(pm, accounts):
    tt = pm('dao-envelop/envelop-protocol-v1@1.1.0').TechTokenV1
    yield tt.deploy({'from':  accounts[0]})

@pytest.fixture(scope="module")
def wrapperTrustedV1(accounts, TrustedWrapperV2, techERC20ForSaftV2, saftv2):
    tw = accounts[0].deploy(TrustedWrapperV2, 
        techERC20ForSaftV2.address, saftv2.address
    )
    yield tw
    # tw = pm('dao-envelop/envelop-protocol-v1@1.1.0').TrustedWrapper
    # #t.setTokenService(tokenService.address, {'from':accounts[0]})
    # yield tw.deploy(techERC20ForSaftV2.address, saftv2.address, {'from':  accounts[0]}) 

@pytest.fixture(scope="module")
def wrapperTrustedV2(accounts, TrustedWrapperV2,techERC20ForSaftV2, saftv2):
    tw = accounts[0].deploy(TrustedWrapperV2, 
        techERC20ForSaftV2.address, saftv2.address
    )
    yield tw

@pytest.fixture(scope="module")
def whiteListsForTrustedWrapper(pm, accounts):
    wL = pm('dao-envelop/envelop-protocol-v1@1.1.0').AdvancedWhiteList
    w = accounts[0].deploy(wL)
    yield w  
#-------------------------------------------------------------------


# @pytest.fixture(scope="module")
# def singleAgent(accounts, Agent):
#     a = accounts[0].deploy(Agent)
#     yield a

# @pytest.fixture(scope="module")
# def serviceAndAgent(accounts, ServiceAndAgent, dai, weth, sub_reg):
#     sub_reg.setAssetForPaymentState(dai.address, True, {'from':accounts[0]})
#     sub_reg.setAssetForPaymentState(zero_address, True, {'from':accounts[0]})
#     #sub_reg.setAssetForPaymentState(weth.address, True, {'from':accounts[0]})
#     s = accounts[0].deploy(ServiceAndAgent,sub_reg, dai)
#     yield s
