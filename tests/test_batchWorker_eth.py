import pytest
import logging
from brownie import chain, Wei, reverts
LOGGER = logging.getLogger(__name__)
from web3 import Web3

ORIGINAL_NFT_IDs = [10000,11111,22222, 33333, 44444]
zero_address = '0x0000000000000000000000000000000000000000'
PRICE = 1e17
call_amount = 1e18
eth_amount = 1e18
transfer_fee_amount = 100

#
def test_prepare_tickets(accounts,saftv2, sub_reg):
    sub_reg.setAssetForPaymentState(zero_address, True, {'from':accounts[0]})
    assert sub_reg.whiteListedForPayments(zero_address) == True
    payOptions = [(zero_address, PRICE, 200),]
    subscriptionType = (0,0,1,True, accounts[3])
    tarif = (subscriptionType, payOptions)
    # Add  atriff
    saftv2.newTariff(tarif, {'from':accounts[0]})
    saftv2.authorizeAgentForService(accounts[5],[0],{'from':accounts[0]})
    sub_reg.buySubscription(
        saftv2.address, 0, 0, 
        accounts[0],
        accounts[0],
        {'from':accounts[5], 'value': 1e18}
    )

    #auth agent

def test_wrap(accounts, erc721mock, wrapperTrustedV2, wnft721, niftsy20, saftv2, techERC20ForSaftV2):
    #make wrap NFT with empty
    in_type = 3
    out_type = 3
    in_nft_amount = 3

    #make 721 token for wrapping
    [erc721mock.mint(x, {'from':accounts[0]})  for x in ORIGINAL_NFT_IDs]
    

    if (wrapperTrustedV2.lastWNFTId(out_type)[1] == 0):
        wrapperTrustedV2.setWNFTId(out_type, wnft721.address, 0, {'from':accounts[0]})
    wnft721.setMinter(wrapperTrustedV2.address, {"from": accounts[0]})

    token_property = (in_type, erc721mock.address)

    for i in range(5):
        erc721mock.approve(wrapperTrustedV2, ORIGINAL_NFT_IDs[i], {"from": accounts[0]})
    
    inDataS = []
    receiverS = []
    fee = [('0x00', transfer_fee_amount, niftsy20.address)]
    lock = [('0x0', chain.time() + 100)]
    royalty = [(accounts[9].address, 2000), (wrapperTrustedV2.address, 8000)]
    for i in range(5):

        token_data = (token_property, ORIGINAL_NFT_IDs[i], 0)

        wNFT = ( token_data,
            zero_address,
            fee,
            lock,
            royalty,
            out_type,
            0,
            Web3.toBytes(0x0000)
            )
        inDataS.append(wNFT)

        receiverS.append(accounts[i].address)

    eth_data = ((1, zero_address), 0, 1e18)
    collateralS = [eth_data]


    #set wrapper for batchWorker
    saftv2.setTrustedWrapper(wrapperTrustedV2, {"from": accounts[0]})

    #wrap batch
    tx = saftv2.wrapBatch(inDataS, collateralS, receiverS, {"from": accounts[0], "value": len(ORIGINAL_NFT_IDs)*eth_amount})
    
    #check WrappedV1 events - eth
    for i in range(len(tx.events['WrappedV1'])):
        assert tx.events['WrappedV1'][i]['nativeCollateralAmount'] == eth_amount
        assert wrapperTrustedV2.getWrappedToken(tx.events['WrappedV1'][i]['outAssetAddress'], tx.events['WrappedV1'][i]['outTokenId'])[1][0][2] == eth_amount
    assert wrapperTrustedV2.balance() == len(ORIGINAL_NFT_IDs)*eth_amount
