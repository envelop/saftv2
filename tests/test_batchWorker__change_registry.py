import pytest
import logging
from brownie import chain, Wei, reverts
LOGGER = logging.getLogger(__name__)
from web3 import Web3


PRICE = 1e18
zero_address = '0x0000000000000000000000000000000000000000'

#service provider has Agent. Buy ticket for ether and call agent buySubscription method. With Agent. Ticket is with expiring time
#then subscription registry is being changed. Try to use old ticket
def test_buy_subscription(accounts, dai, weth, sub_reg, saftv2, agent, sub_reg1, wnft721, wrapperTrustedV1):

	#settings
	if (wrapperTrustedV1.lastWNFTId(3)[1] == 0):
		wrapperTrustedV1.setWNFTId(3, wnft721.address, 0, {'from':accounts[0]})
	wnft721.setMinter(wrapperTrustedV1.address, {"from": accounts[0]})
	#set wrapper for batchWorker
	saftv2.setTrustedWrapper(wrapperTrustedV1, {"from": accounts[0]})



	payOptions = [(dai, PRICE, 100), (zero_address, PRICE/5, 100)] #with Agent fee!!!
	subscriptionType = (0,100,0,True, accounts[3]) #time subscription
	tariff1 = (subscriptionType, payOptions)

	#add tokens to whiteList
	sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
	sub_reg.setAssetForPaymentState(zero_address, True, {'from':accounts[0]})

	#register tariffs for service
	saftv2.newTariff(tariff1,{'from':accounts[0]})
	#register agent - self service Provider
	saftv2.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})
	
	pay_amount = payOptions[1][1]*(sub_reg.PERCENT_DENOMINATOR()+sub_reg.platformFeePercent() + payOptions[1][2])/sub_reg.PERCENT_DENOMINATOR()

	#pay for ether 
	agent.buySubscription(saftv2.address, 0, 1, accounts[1], accounts[1], {"from": accounts[1], "value": pay_amount})

	ticket = saftv2.checkUser(accounts[1])
	logging.info(ticket)
	assert ticket[0] == True
	assert ticket[1] == False

	
	########## change subscription registry ########
	
	with reverts("Ownable: caller is not the owner"):
		sub_reg1.setPreviousRegistry(sub_reg.address, {"from": accounts[1]})
	sub_reg1.setPreviousRegistry(sub_reg.address, {"from": accounts[0]})

	with reverts("Ownable: caller is not the owner"):
		sub_reg.setProxyRegistry(sub_reg1.address, {"from": accounts[1]})
	sub_reg.setProxyRegistry(sub_reg1.address, {"from": accounts[0]})

	with reverts("Ownable: caller is not the owner"):
		saftv2.setSubscriptionRegestry(sub_reg1.address, {"from": accounts[1]})
	saftv2.setSubscriptionRegestry(sub_reg1.address, {"from": accounts[0]})

	#subscription registry is new but user uses ticket of old subscription
	wNFT = ( ((0, zero_address), 0,0),
            zero_address,
            [],
            [],
            [],
            3,
            0,
            Web3.toBytes(0x0000)
        )
	tx = saftv2.wrapBatch([wNFT], [] , [accounts[1]], {"from": accounts[1]})

	chain.sleep(150)
	chain.mine()

	ticket = saftv2.checkUser(accounts[1])
	logging.info(ticket)
	assert ticket[0] == False
	assert ticket[1] == False




	#try to buy subscription - new subscription registry, there are not registered tariffs in new subscription registry
	with reverts("Index out of range"):
		agent.buySubscription(saftv2.address, 0, 1, accounts[1], accounts[1], {"from": accounts[1], "value": pay_amount})

	payOptions = [(dai, PRICE/7, 100), (zero_address, PRICE/15, 100)] #with Agent fee!!!
	subscriptionType = (0,0,3,True, accounts[3])  #count subscription
	tariff1 = (subscriptionType, payOptions)
	#add tokens to whiteList
	sub_reg1.setAssetForPaymentState(dai, True, {'from':accounts[0]})
	sub_reg1.setAssetForPaymentState(zero_address, True, {'from':accounts[0]})

	#register tariffs for service
	saftv2.newTariff(tariff1,{'from':accounts[0]})
	#register agent - separate agent
	saftv2.authorizeAgentForService(agent.address, [0],{"from": accounts[0]})

	#subscription is expired. Buy new count subscription
	pay_amount = payOptions[1][1]*(sub_reg1.PERCENT_DENOMINATOR()+sub_reg1.platformFeePercent() + payOptions[1][2])/sub_reg1.PERCENT_DENOMINATOR()
	agent.buySubscription(saftv2.address, 0, 1, accounts[1], accounts[1], {"from": accounts[1], "value": pay_amount})

	ticket = saftv2.checkUser(accounts[1])
	assert ticket[0] == True
	assert ticket[1] == True



