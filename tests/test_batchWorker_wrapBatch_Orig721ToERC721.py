import pytest
import logging
from brownie import chain, Wei, reverts
LOGGER = logging.getLogger(__name__)
from web3 import Web3

ORIGINAL_NFT_IDs = [10000,11111,22222, 33333, 44444] 
zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = 1e18
transfer_fee_amount = 100
PRICE = 1e18

def test_prepare_tickets(accounts,saftv2, sub_reg, agent, weth, dai, wnft721, wrapperTrustedV2):
    sub_reg.setAssetForPaymentState(zero_address, True, {'from':accounts[0]})
    sub_reg.setAssetForPaymentState(dai, True, {'from':accounts[0]})
    sub_reg.setAssetForPaymentState(weth, True, {'from':accounts[0]})
    assert sub_reg.whiteListedForPayments(zero_address) == True
    assert sub_reg.whiteListedForPayments(dai) == True
    assert sub_reg.whiteListedForPayments(weth) == True
    
    #tariffs for separate Agent
    payOptions1 = [(dai.address, 10*PRICE, 100), (weth.address, 3*PRICE, 100), (zero_address, PRICE, 200),]
    subscriptionType1 = (0,0,1,True, accounts[3]) #with count
    tarif1 = (subscriptionType1, payOptions1)
    
    payOptions2 = [(dai.address, 5*PRICE, 100), (weth.address, 2*PRICE, 100), (zero_address, PRICE, 200),]
    subscriptionType2 = (200,100,0,True, accounts[3]) #with time ticket and time lock
    tarif2 = (subscriptionType2, payOptions2)

    # Add  atriff
    saftv2.newTariff(tarif1, {'from':accounts[0]})
    saftv2.newTariff(tarif2, {'from':accounts[0]})


    saftv2.authorizeAgentForService(agent,[0,1],{'from':accounts[0]})

    pay_amount = payOptions1[0][1]*(sub_reg.PERCENT_DENOMINATOR() + sub_reg.platformFeePercent()+payOptions1[0][2])/sub_reg.PERCENT_DENOMINATOR()
    dai.transfer(accounts[1], pay_amount, {"from": accounts[0]})
    dai.approve(sub_reg, pay_amount, {"from": accounts[1]})

    #buy for erc20 tokens
    agent.buySubscription(
        saftv2.address, 0, 0, 
        accounts[1],
        accounts[1],
        {'from':accounts[1]}
    )
    logging.info(saftv2.checkUser(accounts[1]))

    pay_amount = payOptions1[2][1]*(sub_reg.PERCENT_DENOMINATOR() + sub_reg.platformFeePercent()+payOptions1[2][2])/sub_reg.PERCENT_DENOMINATOR()
    #buy for ether
    agent.buySubscription(
        saftv2.address, 0, 2, 
        accounts[2],
        accounts[2],
        {'from':accounts[2], "value": pay_amount}
    )
    

#test with subscription
def test_wrap(accounts, erc721mock, wrapperTrustedV2, dai, weth, wnft721, niftsy20, saftv2, whiteListsForTrustedWrapper, techERC20ForSaftV2, sub_reg):
    #make wrap NFT with empty
    in_type = 3
    out_type = 3
    in_nft_amount = 3

    #set wrapper for batchWorker
    saftv2.setTrustedWrapper(wrapperTrustedV2, {"from": accounts[0]})

    #add whiteList
    wrapperTrustedV2.setWhiteList(whiteListsForTrustedWrapper.address, {"from": accounts[0]})

    #add tokens in whiteList (dai and niftsy). Weth is NOT in whiteList
    wl_data = (True, True, False, techERC20ForSaftV2.address)
    whiteListsForTrustedWrapper.setWLItem((2, dai.address), wl_data, {"from": accounts[0]})
    whiteListsForTrustedWrapper.setWLItem((2, niftsy20.address), wl_data, {"from": accounts[0]})

    if (wrapperTrustedV2.lastWNFTId(out_type)[1] == 0):
        wrapperTrustedV2.setWNFTId(out_type, wnft721.address, 0, {'from':accounts[0]})
    wnft721.setMinter(wrapperTrustedV2.address, {"from": accounts[0]})

    #make 721 token for wrapping
    [erc721mock.mint(x, {'from':accounts[1]})  for x in ORIGINAL_NFT_IDs]
    
    token_property = (in_type, erc721mock.address)
    dai_property = (2, dai.address)
    weth_property = (2, weth.address)

    for i in range(len(ORIGINAL_NFT_IDs)):
        erc721mock.approve(wrapperTrustedV2, ORIGINAL_NFT_IDs[i], {"from": accounts[1]})

    logging.info(erc721mock.getApproved(ORIGINAL_NFT_IDs[0]))
        
    dai_amount = 0
    weth_amount = 0
    inDataS = []
    receiverS = []
    fee = [('0x00', transfer_fee_amount, niftsy20.address)]
    lock = [('0x0', chain.time() + 100)]
    royalty = [(accounts[9].address, 2000), (wrapperTrustedV2.address, 8000)]
    for i in range(len(ORIGINAL_NFT_IDs)):

        token_data = (token_property, ORIGINAL_NFT_IDs[i], 0)

        wNFT = ( token_data,
            zero_address,
            fee,
            lock,
            royalty,
            out_type,
            0,
            Web3.toBytes(0x0000)
            )
        inDataS.append(wNFT)

        dai_amount = dai_amount + Wei(call_amount)
        weth_amount = weth_amount + Wei(2*call_amount)

        receiverS.append(accounts[i].address)

    dai_data = (dai_property, 0, Wei(call_amount))
    weth_data = (weth_property, 0, Wei(2*call_amount))
    eth_data = ((1, zero_address), 0, 1e18)
    collateralS = [eth_data, dai_data, weth_data]
    #collateralS = [dai_data, weth_data]
    dai.transfer(accounts[1],dai_amount, {"from": accounts[0]} )
    weth.transfer(accounts[1],weth_amount, {"from": accounts[0]} )
    dai.approve(wrapperTrustedV2.address, dai_amount, {"from": accounts[1]})
    weth.approve(wrapperTrustedV2.address, weth_amount, {"from": accounts[1]})

    #wrap batch
    tx = saftv2.wrapBatch(inDataS, collateralS , receiverS, {"from": accounts[1], "value": len(ORIGINAL_NFT_IDs)*eth_amount})
    
    logging.info(tx.gas_used)

    
    #check WrappedV1 events
    for i in range(len(tx.events['WrappedV1'])):
        assert tx.events['WrappedV1'][i]['inAssetAddress'] == erc721mock.address
        assert tx.events['WrappedV1'][i]['outAssetAddress'] == wnft721.address
        assert tx.events['WrappedV1'][i]['inAssetTokenId'] == ORIGINAL_NFT_IDs[i]
        assert tx.events['WrappedV1'][i]['outTokenId'] == i+1
        assert tx.events['WrappedV1'][i]['wnftFirstOwner'] == accounts[i]
        assert tx.events['WrappedV1'][i]['nativeCollateralAmount'] == eth_amount
        assert tx.events['WrappedV1'][i]['rules'] == '0x0000'

    #check CollateralAdded events
    for i in range(len(tx.events['CollateralAdded'])):
        assert tx.events['CollateralAdded'][i]['wrappedAddress'] == wnft721.address
        assert tx.events['CollateralAdded'][i]['wrappedId'] in [1,2,3,4,5]
        if tx.events['CollateralAdded'][i]['collateralAddress'] == zero_address:
            assert tx.events['CollateralAdded'][i]['collateralBalance'] == eth_amount
            assert tx.events['CollateralAdded'][i]['assetType'] == 1
        elif tx.events['CollateralAdded'][i]['collateralAddress'] == dai.address:
            assert tx.events['CollateralAdded'][i]['collateralBalance'] == call_amount
            assert tx.events['CollateralAdded'][i]['assetType'] == 2
        elif tx.events['CollateralAdded'][i]['collateralAddress'] == weth.address:
            assert tx.events['CollateralAdded'][i]['collateralBalance'] == 2*call_amount
            assert tx.events['CollateralAdded'][i]['assetType'] == 2
        else:
            assert True == False
        assert tx.events['CollateralAdded'][i]['collateralTokenId'] == 0
    
    for i in range(len(ORIGINAL_NFT_IDs)):
        #check collateral in wnft
        assert wnft721.wnftInfo(i+1)[1][0] == eth_data
        assert wnft721.wnftInfo(i+1)[1][1] == dai_data
        assert wnft721.wnftInfo(i+1)[1][2] == weth_data

        #check wnft data
        assert wnft721.wnftInfo(i+1)[0][0] == token_property
        assert wnft721.wnftInfo(i+1)[0][1] == ORIGINAL_NFT_IDs[i]
        assert wnft721.wnftInfo(i+1)[0][2] == 0
        assert wnft721.wnftInfo(i+1)[2] == zero_address
        assert wnft721.wnftInfo(i+1)[3] == fee
        assert wnft721.wnftInfo(i+1)[4] == lock
        assert wnft721.wnftInfo(i+1)[5] == royalty
        assert wnft721.wnftInfo(i+1)[6] == '0x0000'
        
        #check owner of nft
        assert erc721mock.ownerOf(ORIGINAL_NFT_IDs[i]) == wrapperTrustedV2.address
        assert wnft721.ownerOf(i+1) == accounts[i]

    assert dai.balanceOf(wrapperTrustedV2.address) == call_amount*len(ORIGINAL_NFT_IDs)
    assert weth.balanceOf(wrapperTrustedV2.address) == 2*call_amount*len(ORIGINAL_NFT_IDs)
    assert wrapperTrustedV2.balance() == eth_amount*len(ORIGINAL_NFT_IDs)

    #make transfer with fee
    for i in [1,2,3,4]:
        niftsy20.transfer(accounts[i], transfer_fee_amount, {"from": accounts[0]})
        niftsy20.approve(wrapperTrustedV2.address, transfer_fee_amount, {"from": accounts[i]})
        before_balance_acc = niftsy20.balanceOf(accounts[9])
        before_balance_wrapper = niftsy20.balanceOf(wrapperTrustedV2.address)
        wnft721.transferFrom(accounts[i], accounts[0], i+1, {"from": accounts[i]})
        assert niftsy20.balanceOf(accounts[i]) == 0
        assert niftsy20.balanceOf(wrapperTrustedV2.address) == before_balance_wrapper + transfer_fee_amount*royalty[1][1]/10000
        assert niftsy20.balanceOf(accounts[9]) == before_balance_acc + transfer_fee_amount*royalty[0][1]/10000
        wnft721.ownerOf(i+1) == accounts[0]

    #try to add collateral (allowed and not allowed tokens)
    with reverts("WL:Some assets are not enabled for collateral"):
        wrapperTrustedV2.addCollateral(wnft721.address, 1, [weth_data], {"from": accounts[0]})

    #add collateral
    for i in range(len(ORIGINAL_NFT_IDs)):
        dai.approve(wrapperTrustedV2.address, call_amount, {"from": accounts[0]})
        wrapperTrustedV2.addCollateral(wnft721.address, i+1, [dai_data], {"from": accounts[0]})

    assert dai.balanceOf(wrapperTrustedV2.address) == 2*call_amount * len(ORIGINAL_NFT_IDs)        

    #try to unwrap - failed. Timelock

    with reverts("TimeLock error"):
        wrapperTrustedV2.unWrap(wnft721.address, 1, {"from": accounts[0]})

    chain.sleep(100)
    chain.mine()

    before_balance_acc_dai = dai.balanceOf(accounts[0])
    before_balance_acc_weth = weth.balanceOf(accounts[0])
    before_balance_acc_niftsy = niftsy20.balanceOf(accounts[0])
    before_balance_acc_eth = accounts[0].balance()

    #check balances after UNWRAP
    for i in range(len(ORIGINAL_NFT_IDs)):
        wrapperTrustedV2.unWrap(wnft721.address, i+1, {"from": accounts[0]})
        assert erc721mock.ownerOf(ORIGINAL_NFT_IDs[i]) == accounts[0]

    assert dai.balanceOf(accounts[0]) == before_balance_acc_dai + 2*call_amount * len(ORIGINAL_NFT_IDs)
    assert weth.balanceOf(accounts[0]) == before_balance_acc_weth + 2*call_amount * len(ORIGINAL_NFT_IDs)
    assert niftsy20.balanceOf(accounts[0]) == before_balance_acc_niftsy + (transfer_fee_amount*royalty[1][1]/10000) * (len(ORIGINAL_NFT_IDs)-1)
    assert accounts[0].balance() == before_balance_acc_eth + eth_amount*len(ORIGINAL_NFT_IDs)
    assert dai.balanceOf(wrapperTrustedV2.address) == 0
    assert weth.balanceOf(wrapperTrustedV2.address) == 0
    assert wrapperTrustedV2.balance() == 0

    #try to use service again (count lefts finishes)

    wNFT = ( ((0, zero_address), 0,0),
            zero_address,
            fee,
            lock,
            royalty,
            out_type,
            0,
            Web3.toBytes(0x0000)
        )

    with reverts("Valid ticket not found"):
        tx = saftv2.wrapBatch([wNFT], [] , [accounts[1]], {"from": accounts[1]})

    tx = saftv2.wrapBatch([wNFT], [] , [accounts[2]], {"from": accounts[2]})

    assert sub_reg.getUserTicketForService(saftv2.address, accounts[2])[0] <= chain.time()
    assert sub_reg.getUserTicketForService(saftv2.address, accounts[2])[1] == 0

def test_prepare_tickets_1(accounts,saftv2, sub_reg, agent, weth, dai, wnft721, wrapperTrustedV2, whiteListsForTrustedWrapper, techERC20ForSaftV2):
    #set wrapper
    sub_reg.setMainWrapper(wrapperTrustedV2.address, {"from": accounts[0]})
    payOptions2 = [(dai.address, 5*PRICE, 100), (weth.address, 2*PRICE, 100), (zero_address, PRICE, 200),]

    #add tokens in whiteList (dai and niftsy). Weth is NOT in whiteList
    wl_data = (True, True, False, techERC20ForSaftV2.address)
    whiteListsForTrustedWrapper.setWLItem((2, weth.address), wl_data, {"from": accounts[0]})

    pay_amount = payOptions2[1][1]
    weth.transfer(accounts[3], pay_amount, {"from": accounts[0]})
    weth.approve(sub_reg.address, pay_amount, {"from": accounts[3]})
    #inData = (((0, zero_address), 0,0),zero_address,[],[],[],3,0,0x0000);

    #buy for wrapping
    agent.buySubscription(
        saftv2.address, 1, 1, 
        accounts[3],
        accounts[3],
        {'from':accounts[3]}
    )

    wToken = wrapperTrustedV2.lastWNFTId(3)[1]
    assert wnft721.ownerOf(wToken) == accounts[3]
    
    assert saftv2.checkUser(accounts[3])[0] == True

    wNFT = ( 
                ((0, zero_address), 0,0),
                zero_address,
                [],
                [],
                [],
                3,
                0,
                Web3.toBytes(0x0000)
            )

    #check working of subscription
    before_acc1 = wnft721.balanceOf(accounts[1])
    before_acc2 = wnft721.balanceOf(accounts[2])
    tx = saftv2.wrapBatch([wNFT, wNFT], [] , [accounts[1], accounts[2]], {"from": accounts[3]})

    assert wnft721.balanceOf(accounts[1]) == before_acc1+1
    assert wnft721.balanceOf(accounts[2]) == before_acc2+1

    assert sub_reg.getUserTicketForService(saftv2, accounts[3])[0] > chain.time()
    assert sub_reg.getUserTicketForService(saftv2, accounts[3])[1] == 0

