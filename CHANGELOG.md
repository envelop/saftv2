
# CHANGELOG

All notable changes to this project are documented in this file.

This changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Factory Example

## [2.0.0](https://github.com/dao-envelop/subscription/tree/2.0.0) - 2023-05-22
### Added
- Upgrade solidity version up to 0.8.19
- Open Zeppelin dependencies upgrade to 4.8.3
- EnvelopV1 dependencies upgrade to 1.2.0


### Fixed
- CHANGELOG started

