from brownie import *
import json
import logging
LOGGER = logging.getLogger(__name__)

accounts.load("secret2")

zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = "1 ether"
nft_ids = [10000, 20000, 30000, 44, 55, 77]
PRICE = 1e18
day = 24*60*60

def main():


	#poligon main
	saftv2 = BatchWorkerV2.at('0xFb453ebA20Dc4598bb77A22747B0BCc971B5630B')
	#agent = EnvelopAgentWithRegistry.at("0x7C20A3E0f58304D33074759D220DE11b9DF8014f")
	#sub_reg = SubscriptionRegistry.at("0x937cc2f0e4E40Ebe774aFd01911e3D14B9cd21c0")

	tariffs = []

	#time tariffs - direct payment - no agent fee
	'''payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 1e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 1e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 1e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,1*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 1e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 1e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 1e6, 0) #check decimals
				]

	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,1,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))'''




	'''payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 15e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 15e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 15e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,1*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 60e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 60e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 60e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,7*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 250e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 250e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 250e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,60*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 600e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 600e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 600e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,180*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 1000e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 1000e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 1000e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,365*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))



############################################################################################################



	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 15e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 15e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 15e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,1,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 60e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 60e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 60e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,5,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 200e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 200e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 200e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,20,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 600e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 600e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 600e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,100,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 1000e18, 0), #check decimals
					("0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 1000e6, 0), #check decimals
					("0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 1000e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,500,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))'''

	'''payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 4500e18, 0) #check decimals
				
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (30*day,1*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 18000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (90*day,7*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))'''

	'''payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 75000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (180*day,60*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 180000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (365*day,180*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 300000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (550*day,365*day,0,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))



############################################################################################################



	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 4500e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (30*day,0,1,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))'''

	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 18000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (90*day,0,5,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	'''payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 75000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (180*day,0,20,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 180000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (365*day,0,100,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 300000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (550*day,0,500,True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6")
	tariffs.append((subscriptionType, payOptions))'''

	# Add  atriff
	for i in tariffs:
		print(i)
		saftv2.newTariff(i, {'from':accounts[0]})

	#change start number in range!!!
	#change agent address!!!


	#polygon-main
	#for test tariffs
	#saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",list(range(0, len(tariffs))),{'from':accounts[0]})
	#for prod tariffs
	#saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",list(range(2, len(tariffs)+2)),{'from':accounts[0]})
	saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21],{'from':accounts[0]})
	#print(list(range(2, len(tariffs))))

