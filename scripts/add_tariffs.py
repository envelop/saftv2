from brownie import *
import json
import logging
LOGGER = logging.getLogger(__name__)

accounts.load("secret2")

zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = "1 ether"
nft_ids = [10000, 20000, 30000, 44, 55, 77]
PRICE = 1e18
day = 24*60*60

def main():

	#bsc main
	saftv2 = BatchWorkerV2.at('0xC17302F7b6cb7f8567d95748Cf9a5e1A15c0c25b')
	#agent = EnvelopAgentWithRegistry.at("0x7C20A3E0f58304D33074759D220DE11b9DF8014f")
	#sub_reg = SubscriptionRegistry.at("0x5385e7E022031013D46654d20937DE6bE6Ea51BE")

	tariffs = []

	#time tariffs - direct payment - no agent fee
	'''payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 1e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 1e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 1e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 1e18, 0)	 #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,1*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 1e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 1e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 1e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 1e18, 0)  #check decimals
				]

	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,1,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))'''


	#direct payment way
	'''payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 15e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 15e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 15e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 15e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,1*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 60e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 60e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 60e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 60e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,7*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 250e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 250e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 250e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 250e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,60*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 600e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 600e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 600e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 600e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,180*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 1000e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 1000e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 1000e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 1000e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,365*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))



############################################################################################################



	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 15e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 15e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 15e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 15e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,1,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 60e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 60e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 60e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 60e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,5,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 200e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 200e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 200e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 200e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,20,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 600e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 600e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 600e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 600e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,100,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 1000e18, 0), #check decimals
					("0x55d398326f99059fF775485246999027B3197955", 1000e18, 0), #check decimals
					("0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 1000e18, 0), #check decimals
					("0xe9e7cea3dedca5984780bafc599bd69add087d56", 1000e18, 0)  #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,500,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))'''

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 4500e18, 0) #check decimals
				
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (30*day,1*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 18000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (90*day,7*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 75000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (180*day,60*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 180000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (365*day,180*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 300000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (550*day,365*day,0,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))



############################################################################################################



	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 4500e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (30*day,0,1,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 18000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (90*day,0,5,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 75000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (180*day,0,20,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 180000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (365*day,0,100,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 300000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (550*day,0,500,True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021")
	tariffs.append((subscriptionType, payOptions))

	# Add  atriff
	for i in tariffs:
	#	print(i)
		saftv2.newTariff(i, {'from':accounts[0]})

	#change start number in range!!!
	#change agent address!!!

	#bsc-main
	#for test tariffs
	#saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",list(range(0, len(tariffs))),{'from':accounts[0]})
	#for prod tariffs
	#saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",list(range(2, len(tariffs)+2)),{'from':accounts[0]})
	saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],{'from':accounts[0]})
	
	#print(list(range(2, len(tariffs))))

