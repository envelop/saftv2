from brownie import *
import json

if  web3.eth.chain_id in [4, 5, 97, 1313161555]:
    # Testnets
    #private_key='???'
    accounts.load('ttwo');
elif web3.eth.chain_id in [1,56,137, 42161, 1313161554]:
    accounts.load('envdeployer')
    
    pass

print('Deployer:{}, balance: {}'.format(accounts[0],Wei(accounts[0].balance()).to('ether') ))
print('web3.eth.chain_id={}'.format(web3.eth.chainId))
ARBITRUM_MAIN_ERC20_COLLATERAL_TOKENS = [
'0x120e49d7ab1EDc0bFBF509Fa8566ca5b5dCAAd40',  #NIFTSY
'0xFd086bC7CD5C481DCC9C85ebE478A1C0b69FCbb9',  #USDT
'0x0000000000000000000000000000000000000000'
]

CHAIN = {   
    0:{'explorer_base':'io'},
    1:{'explorer_base':'etherscan.io', 
       #'enabled_erc20': ETH_MAIN_ERC20_COLLATERAL_TOKENS,
       'niftsy': '0x7728cd70b3dD86210e2bd321437F448231B81733',
       'techERC20':'0x41FF1Dbe71cD32AF87E1eC88dcA8C12045cd0CA9',
       'wl':'0xF99796325c62984092B81778E4B4526daA7DE752'
    },
    4:{'explorer_base':'rinkeby.etherscan.io',
        #'enabled_erc20': ETH_RINKEBY_ERC20_COLLATERAL_TOKENS,
        'niftsy': '0x3125B3b583D576d86dBD38431C937F957B94B47d'
    },
    5:{'explorer_base':'goerli.etherscan.io',
       #'enabled_erc20': ETH_GOERLI_ERC20_COLLATERAL_TOKENS,
       'niftsy': '0x376e8EA664c2E770E1C45ED423F62495cB63392D'
    },
    56:{'explorer_base':'bscscan.com', 
        #'enabled_erc20': BSC_MAIN_ERC20_COLLATERAL_TOKENS,
        'niftsy': '',
        'techERC20':'0x7716Bf599131aCC5169d8F2369375632B56Cef7D',
        'wl':'0x0fE8B380723cc75d75D49a9e3D8811A8E114246D'
    },
    97:{'explorer_base':'testnet.bscscan.com', 
        #'enabled_erc20': BSC_TESTNET_ERC20_COLLATERAL_TOKENS
    },
    137:{'explorer_base':'polygonscan.com', 
         #'enabled_erc20': POLYGON_MAIN_ERC20_COLLATERAL_TOKENS,
         'niftsy': '',
         'techERC20':'0x70D06cD502BC51fa7C805677c6ecB7e43cddE439',
         'wl':'0x1A9DF09bb68ED345fFAAf515991C098cCA3B2BB9'
    },
    80001:{'explorer_base':'mumbai.polygonscan.com', 
    },

    42161:{'explorer_base':'arbiscan.io', 
       'enabled_erc20': ARBITRUM_MAIN_ERC20_COLLATERAL_TOKENS,
       'niftsy': '0x120e49d7ab1EDc0bFBF509Fa8566ca5b5dCAAd40',
       'techERC20':'0xE6b10041D6d5b3aa7767681644CF71b5a8F96E06',
       'wl':'0xd142359762b3DfD45CC66B0e7C47A75Fe4c34464'
    },

    43114:{'explorer_base':'cchain.explorer.avax.network', 
           #'enabled_erc20': AVALANCHE_MAIN_ERC20_COLLATERAL_TOKENS
    },
    43113:{'explorer_base':'cchain.explorer.avax-test.network', 
    },

}.get(web3.eth.chainId, {'explorer_base':'io','niftsy': '',
         'techERC20':'0x70D06cD502BC51fa7C805677c6ecB7e43cddE439',
         'wl':'0x70D06cD502BC51fa7C805677c6ecB7e43cddE439'}
)

tx_params = {'from':accounts[0]}
if web3.eth.chainId in  [1,4, 5, 137, 42161]:
    tx_params={'from':accounts[0], 'priority_fee': chain.priority_fee}


#sub_reg_addr = '0x937cc2f0e4E40Ebe774aFd01911e3D14B9cd21c0'
#depends = config["dependencies"][1]
#print(depends)
#p = project.load(depends)
#TechTokenV1 = p.TechTokenV1
#EnvelopwNFT1155 = p.EnvelopwNFT1155
#EnvelopwNFT721 = p.EnvelopwNFT721

#techERC20 = TechTokenV1.deploy(tx_params)
#techERC20 = TechTokenV1.at('0x6E3778f165C2c6528fBD49B210d5158cCdfF517d')
#wnft1155 = EnvelopwNFT1155.at('0x663F4270BDA4b8b88380d3D6E15f91BA7D827d0D')

# wnft1155 = EnvelopwNFT1155.deploy(
#         'ENVELOP 1155 wNFT SAFT V2 Collection', 
#         'swNFT', 
#         'https://api.envelop.is/metadata/',
#         wrapper,
#         tx_params,
#         publish_source=True
#     )

#wnft721 = EnvelopwNFT721.at('0xB0C9AEAe56A4D3F50ec7d9c19d2197099a0684fA')    
# wnft721 = EnvelopwNFT721.deploy(
#         'ENVELOP 721 wNFT SAFT V2 Collection', 
#         'swNFT', 
#         'https://api.envelop.is/metadata/',
#         wrapper,
#         tx_params,
#         publish_source=True
#     )

def main():
    sub_reg_addr = input('\nPlease input Subregistry address: ')
    if len(sub_reg_addr) == 42:
        #saftv2 = BatchWorkerV2.deploy(sub_reg_addr, tx_params)
        saftv2 = BatchWorkerV2.at('0x17105c8faee79e7fD81e8719a0190B86E2527546')
        wrapperTrustedV2 = TrustedWrapperV2.deploy(
        CHAIN['techERC20'], 
        saftv2.address, 
        tx_params,

    )
    
    # Print addresses for quick access from console
    print("----------Deployment artifacts-------------------")
    #print("techERC20 = TechTokenV1.at('{}')".format(CHAIN['techERC20']))
    print("saftv2 = BatchWorkerV2.at('{}')".format(saftv2.address))
    print("sub_reg = SubscriptionRegistry.at('{}')".format(sub_reg_addr))
    print("wrapperTrustedV2 = TrustedWrapperV2.at('{}')".format(wrapperTrustedV2.address))
    #print("wnft1155 = EnvelopwNFT1155.at('{}')".format(wnft1155.address))
    #print("wnft721 = EnvelopwNFT721.at('{}')".format(wnft721.address))

    #print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],CHAIN['techERC20']))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],saftv2.address))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],sub_reg_addr))
    print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],wrapperTrustedV2.address))
    #print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],wnft1155.address))
    #print('https://{}/address/{}#code'.format(CHAIN['explorer_base'],wnft721.address))

    
    #make settings of contracts
    # wnft1155.setMinterStatus(wrapperTrustedV2.address, tx_params)
    # wnft721.setMinter(wrapperTrustedV2.address, tx_params)
    #wrapperTrustedV2.setWNFTId(3, wnft721_address, 1, tx_params)
    #wrapperTrustedV2.setWNFTId(4, wnft1155_address,1, tx_params)

    wrapperTrustedV2.setTrustedOperatorStatus(sub_reg_addr, True)
    wrapperTrustedV2.setTrustedOperatorStatus(saftv2.address, True)
    saftv2.setTrustedWrapper(wrapperTrustedV2.address, tx_params)
    #sub_reg.setAgentStatus(saftV1.address, True, tx_params)

    # #make settings of subscription -- add tarif
    # if  CHAIN.get('niftsy', None) is not None:
    #     subscriptionType = (timelockPeriod, ticketValidPeriod, counter, True)
    #     payOption = [(CHAIN.get('niftsy', None), FREEZ_AMOUNT)]
    #     services = [0]
    #     subscriptionManager.addTarif(
    #         (subscriptionType, payOption, services), 
    #         tx_params
    #     )

    # #make settings of manager
    # subscriptionManager.setMainWrapper(wrapperTrustedV1, tx_params)
    # saftV1.setSubscriptionManager(subscriptionManager.address, tx_params)




    if  web3.eth.chainId in [1,4,5, 56, 137, 42161, 43114]:
        #TechTokenV1.publish_source(techERC20);
        t
        #EnvelopwNFT1155.publish_source(wnft1155);
       # EnvelopwNFT721.publish_source(wnft721);
        TrustedWrapperV2.publish_source(wrapperTrustedV2)

