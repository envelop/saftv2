from brownie import *
import json
import logging
LOGGER = logging.getLogger(__name__)

accounts.load("secret2")

zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = "1 ether"
nft_ids = [10000, 20000, 30000, 44, 55, 77]
PRICE = 1e18
day = 24*60*60

def main():


	#ethereum main
	saftv2 = BatchWorkerV2.at('0xC3cAF772Fc48f25D0C68C507699D95e44CA4E06E')
	#agent = EnvelopAgentWithRegistry.at("0x7C20A3E0f58304D33074759D220DE11b9DF8014f")
	#sub_reg = SubscriptionRegistry.at("0x937cc2f0e4E40Ebe774aFd01911e3D14B9cd21c0")

	tariffs = []

	'''payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 15e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 15e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 15e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,1*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 60e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 60e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 60e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,7*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 250e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 250e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 250e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,60*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 600e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 600e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 600e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,180*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 1000e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 1000e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 1000e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,365*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))



############################################################################################################



	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 15e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 15e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 15e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,1,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 60e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 60e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 60e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,5,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 200e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 200e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 200e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,20,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 600e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 600e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 600e6, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,100,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x6b175474e89094c44da98b954eedeac495271d0f", 1000e18, 0), #check decimals
					("0xdAC17F958D2ee523a2206206994597C13D831ec7", 1000e6, 0), #check decimals
					("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 1000e6, 0) #check decimals

				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (0,0,500,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))'''

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 4500e18, 0) #check decimals
				
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (30*day,1*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 18000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (90*day,7*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 75000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (180*day,60*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 180000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (365*day,180*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 300000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (550*day,365*day,0,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))



############################################################################################################



	#count tariffs - direct payment - no agent fee
	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 4500e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (30*day,0,1,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 18000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (90*day,0,5,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 60000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (180*day,0,20,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 180000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (365*day,0,100,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	payOptions = [
					("0x7728cd70b3dD86210e2bd321437F448231B81733", 300000e18, 0) #check decimals
				]
	#change beneficiary = miltisig!!!!
	subscriptionType = (550*day,0,500,True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d")
	tariffs.append((subscriptionType, payOptions))

	# Add  atriff
	for i in tariffs:
		print(i)
		saftv2.newTariff(i, {'from':accounts[0], 'priority_fee': chain.priority_fee})

	#change start number in range!!!
	#change agent address!!!


	#ethereum-main
	#for test tariffs
	#saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",list(range(0, len(tariffs))),{'from':accounts[0]})
	#for prod tariffs
	#saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",list(range(2, len(tariffs)+2)),{'from':accounts[0]})
	saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],{'from':accounts[0],  'priority_fee': chain.priority_fee})
	#print(list(range(2, len(tariffs))))

