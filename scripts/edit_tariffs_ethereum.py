from brownie import *
import json
import logging
LOGGER = logging.getLogger(__name__)

accounts.load("secret2")

zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = "1 ether"
nft_ids = [10000, 20000, 30000, 44, 55, 77]
PRICE = 1e18
day = 24*60*60

def main():


	#ethereum main
	saftv2 = BatchWorkerV2.at('0xC3cAF772Fc48f25D0C68C507699D95e44CA4E06E')
	#agent = EnvelopAgentWithRegistry.at("0x7C20A3E0f58304D33074759D220DE11b9DF8014f")
	#sub_reg = SubscriptionRegistry.at("0x937cc2f0e4E40Ebe774aFd01911e3D14B9cd21c0")

	0,1,2,3,4,5,6,7,8,9,

	tariffs = []

	'''saftv2.editPayOption(0, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(0, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(0, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(1, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 5e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(1, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 5e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(1, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 5e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(2, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 39e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(2, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 39e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(2, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 39e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(3, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 104e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(3, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 104e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(3, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 104e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(4, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 190e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(4, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 190e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(4, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 190e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	#############################################################################################################################################
	saftv2.editPayOption(5, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(5, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(5, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(6, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 6e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(6, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 6e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(6, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 6e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(7, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 22e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(7, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 22e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(7, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 22e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(8, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 97e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(8, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 97e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(8, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 97e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.editPayOption(9, 0, "0x6b175474e89094c44da98b954eedeac495271d0f", 437e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(9, 1, "0xdAC17F958D2ee523a2206206994597C13D831ec7", 437e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(9, 2, "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48", 437e6, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})


	###1

	saftv2.editPayOption(11, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 1867e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(12, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 13500e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(13, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 34020e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(14, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 57652e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(16, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 2667e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})'''
	saftv2.editPayOption(17, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 9000e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(18, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 37800e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editPayOption(19, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 157950e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	###2
	saftv2.editServiceTariff(11, 30*day, 7*day, 0, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(12, 180*day, 60*day, 0, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(13, 360*day, 180*day, 0, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(14, 600*day, 365*day, 0, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(16, 45*day, 0, 5, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(17, 160*day, 0, 20, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(18, 700*day, 0, 100, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.editServiceTariff(19, 2100*day, 0, 500, True, "0xE0E4EC54ed883d7089895C0e951b4bB8E3c68e9d", {"from": accounts[0], 'priority_fee': chain.priority_fee})
	

	####3
	#####
	saftv2.addPayOption(0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(1, "0x7728cd70b3dD86210e2bd321437F448231B81733", 1333e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(2, "0x7728cd70b3dD86210e2bd321437F448231B81733", 9643e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(3, "0x7728cd70b3dD86210e2bd321437F448231B81733", 24300e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(4, "0x7728cd70b3dD86210e2bd321437F448231B81733", 41180e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	#####4
	saftv2.addPayOption(5, "0x7728cd70b3dD86210e2bd321437F448231B81733", 0, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(6, "0x7728cd70b3dD86210e2bd321437F448231B81733", 1600e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(7, "0x7728cd70b3dD86210e2bd321437F448231B81733", 5400e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(8, "0x7728cd70b3dD86210e2bd321437F448231B81733", 22680e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})
	saftv2.addPayOption(9, "0x7728cd70b3dD86210e2bd321437F448231B81733", 94770e18, 0, {"from": accounts[0], 'priority_fee': chain.priority_fee})

	saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",[0,1,2,3,4,5,6,7,8,9,11,12,13,14,16,17,18,19],{'from':accounts[0], 'priority_fee': chain.priority_fee})
