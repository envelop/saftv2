from brownie import *
import json
import logging
LOGGER = logging.getLogger(__name__)

accounts.load("secret2")

zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = "1 ether"
nft_ids = [10000, 20000, 30000, 44, 55, 77]
PRICE = 1e18
day = 24*60*60

def main():

	#poligon main
	saftv2 = BatchWorkerV2.at('0xFb453ebA20Dc4598bb77A22747B0BCc971B5630B')
	#agent = EnvelopAgentWithRegistry.at("0x7C20A3E0f58304D33074759D220DE11b9DF8014f")
	#sub_reg = SubscriptionRegistry.at("0x937cc2f0e4E40Ebe774aFd01911e3D14B9cd21c0")


	'''saftv2.editPayOption(2, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(2, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(2, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 0, 0, {"from": accounts[0]})

	saftv2.editPayOption(3, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 5e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(3, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 5e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(3, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 5e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(4, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 39e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(4, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 39e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(4, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 39e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(5, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 104e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(5, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 104e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(5, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 104e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(6, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 190e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(6, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 190e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(6, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 190e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(7, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(7, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(7, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 0, 0, {"from": accounts[0]})

	saftv2.editPayOption(8, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 6e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(8, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 6e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(8, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 6e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(9, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 22e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(9, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 22e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(9, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 22e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(10, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 97e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(10, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 97e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(10, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 97e6, 0, {"from": accounts[0]})

	saftv2.editPayOption(11, 0, "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063", 437e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(11, 1, "0xc2132d05d31c914a87c6611c10748aeb04b58e8f", 437e6, 0, {"from": accounts[0]})
	saftv2.editPayOption(11, 2, "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", 437e6, 0, {"from": accounts[0]})'''

	###1



	'''saftv2.editPayOption(13, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 1867e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(14, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 13500e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(15, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 34020e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(16, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 57652e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(18, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 2667e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(19, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 9000e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(20, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 37800e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(21, 0, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 157950e18, 0, {"from": accounts[0]})

	###2
	saftv2.editServiceTariff(13, 30*day, 7*day, 0, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(14, 180*day, 60*day, 0, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(15, 360*day, 180*day, 0, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(16, 600*day, 365*day, 0, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(18, 45*day, 0, 5, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(19, 160*day, 0, 20, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(20, 700*day, 0, 100, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})
	saftv2.editServiceTariff(21, 2100*day, 0, 500, True, "0xDC1Aa8D625ab092fEd8Dc5879348dD77700Ceec6", {"from": accounts[0]})'''
	
	####3
	#####
	saftv2.addPayOption(2, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 0, 0, {"from": accounts[0]})
	saftv2.addPayOption(3, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 1333e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(4, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 9643e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(5, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 24300e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(6, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 41180e18, 0, {"from": accounts[0]})

	#####4
	saftv2.addPayOption(7, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 0, 0, {"from": accounts[0]})
	saftv2.addPayOption(8, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 1600e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(9, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 5400e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(10, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 22680e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(11, "0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1", 94770e18, 0, {"from": accounts[0]})

	saftv2.authorizeAgentForService("0x7C20A3E0f58304D33074759D220DE11b9DF8014f",[2,3,4,5,6,7,8,9,10,11,13,14,15,16,18,19,20,21],{'from':accounts[0]})