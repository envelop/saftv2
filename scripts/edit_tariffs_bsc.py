from brownie import *
import json
import logging
LOGGER = logging.getLogger(__name__)

accounts.load("secret2")

zero_address = '0x0000000000000000000000000000000000000000'
call_amount = 1e18
eth_amount = "1 ether"
nft_ids = [10000, 20000, 30000, 44, 55, 77]
PRICE = 1e18
day = 24*60*60

def main():

	#bsc main
	saftv2 = BatchWorkerV2.at('0xC17302F7b6cb7f8567d95748Cf9a5e1A15c0c25b')
	#agent = EnvelopAgentWithRegistry.at("0x7C20A3E0f58304D33074759D220DE11b9DF8014f")
	#sub_reg = SubscriptionRegistry.at("0x5385e7E022031013D46654d20937DE6bE6Ea51BE")

	'''saftv2.editPayOption(2, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(2, 1, "0x55d398326f99059fF775485246999027B3197955", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(2, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(2, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 0, 0, {"from": accounts[0]})

	saftv2.editPayOption(3, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 5e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(3, 1, "0x55d398326f99059fF775485246999027B3197955", 5e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(3, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 5e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(3, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 5e18, 0, {"from": accounts[0]})

	saftv2.editPayOption(4, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 39e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(4, 1, "0x55d398326f99059fF775485246999027B3197955", 39e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(4, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 39e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(4, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 39e18, 0, {"from": accounts[0]})

	saftv2.editPayOption(5, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 104e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(5, 1, "0x55d398326f99059fF775485246999027B3197955", 104e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(5, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 104e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(5, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 104e18, 0, {"from": accounts[0]})

	saftv2.editPayOption(6, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 190e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(6, 1, "0x55d398326f99059fF775485246999027B3197955", 190e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(6, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 190e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(6, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 190e18, 0, {"from": accounts[0]})


	saftv2.editPayOption(7, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(7, 1, "0x55d398326f99059fF775485246999027B3197955", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(7, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(7, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 0, 0, {"from": accounts[0]})'''

	saftv2.editPayOption(8, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 6e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(8, 1, "0x55d398326f99059fF775485246999027B3197955", 6e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(8, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 6e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(8, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 6e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(8, 4, "0x7728cd70b3dD86210e2bd321437F448231B81733", 1600e18, 0, {"from": accounts[0]})

	saftv2.editPayOption(9, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 22e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(9, 1, "0x55d398326f99059fF775485246999027B3197955", 22e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(9, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 22e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(9, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 22e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(9, 4, "0x7728cd70b3dD86210e2bd321437F448231B81733", 5400e18, 0, {"from": accounts[0]})

	saftv2.editPayOption(10, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 97e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(10, 1, "0x55d398326f99059fF775485246999027B3197955", 97e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(10, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 97e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(10, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 97e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(10, 4, "0x7728cd70b3dD86210e2bd321437F448231B81733", 22680e18, 0, {"from": accounts[0]})

	saftv2.editPayOption(11, 0, "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", 437e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(11, 1, "0x55d398326f99059fF775485246999027B3197955", 437e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(11, 2, "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d", 437e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(11, 3, "0xe9e7cea3dedca5984780bafc599bd69add087d56", 437e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(11, 4, "0x7728cd70b3dD86210e2bd321437F448231B81733", 94770e18, 0, {"from": accounts[0]})

	#####1
	'''saftv2.editPayOption(12, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(13, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 1867e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(14, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 13500e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(15, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 34020e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(16, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 57652e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(17, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 0, 0, {"from": accounts[0]})
	saftv2.editPayOption(18, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 2667e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(19, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 9000e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(20, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 37800e18, 0, {"from": accounts[0]})
	saftv2.editPayOption(21, 0, "0x7728cd70b3dD86210e2bd321437F448231B81733", 157950e18, 0, {"from": accounts[0]})

	#####

	saftv2.editServiceTariff(12, 0, 1*day, 0, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(13, 30*day, 7*day, 0, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(14, 180*day, 60*day, 0, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(15, 360*day, 180*day, 0, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(16, 600*day, 365*day, 0, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(17, 0, 0, 1, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(18, 45*day, 0, 5, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(19, 160*day, 0, 20, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(20, 700*day, 0, 100, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})
	saftv2.editServiceTariff(21, 2100*day, 0, 500, True, "0x721d86E0027c1c9E128c4f935AD80fBc921A9021", {"from": accounts[0]})

	#####
	saftv2.addPayOption(2, "0x7728cd70b3dD86210e2bd321437F448231B81733", 0, 0, {"from": accounts[0]})
	saftv2.addPayOption(3, "0x7728cd70b3dD86210e2bd321437F448231B81733", 1333e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(4, "0x7728cd70b3dD86210e2bd321437F448231B81733", 9643e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(5, "0x7728cd70b3dD86210e2bd321437F448231B81733", 24300e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(6, "0x7728cd70b3dD86210e2bd321437F448231B81733", 41180e18, 0, {"from": accounts[0]})


	####

	saftv2.addPayOption(7, "0x7728cd70b3dD86210e2bd321437F448231B81733", 0, 0, {"from": accounts[0]})
	saftv2.addPayOption(8, "0x7728cd70b3dD86210e2bd321437F448231B81733", 2667e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(9, "0x7728cd70b3dD86210e2bd321437F448231B81733", 9000e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(10, "0x7728cd70b3dD86210e2bd321437F448231B81733", 37800e18, 0, {"from": accounts[0]})
	saftv2.addPayOption(11, "0x7728cd70b3dD86210e2bd321437F448231B81733", 157950e18, 0, {"from": accounts[0]})'''



