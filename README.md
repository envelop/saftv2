## Foundry

**Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.**

Foundry consists of:

-   **Forge**: Ethereum testing framework (like Truffle, Hardhat and DappTools).
-   **Cast**: Swiss army knife for interacting with EVM smart contracts, sending transactions and getting chain data.
-   **Anvil**: Local Ethereum node, akin to Ganache, Hardhat Network.
-   **Chisel**: Fast, utilitarian, and verbose solidity REPL.

## Documentation

https://book.getfoundry.sh/

## Usage

### Build

```shell
$ forge build
```

### Test

```shell
$ forge test
```

### Format

```shell
$ forge fmt
```

### Gas Snapshots

```shell
$ forge snapshot
```
0.000253867
### Anvil

```shell
$ anvil
```

### Deploy
### Blast Sepolia
```shell
$ forge script script/Deploy.s.sol:DeployScript --rpc-url blast_sepolia  --account ttwo --sender 0xDDA2F2E159d2Ce413Bd0e1dF5988Ee7A803432E3 --broadcast
```

### Blast Mainnet
```shell
$ forge script script/DeployBlast.s.sol:DeployScriptBlast --rpc-url blast_mainnet  --account envdeployer --sender 0xE1a8F0a249A87FDB9D8B912E11B198a2709D6d9B --broadcast  --verify --priority-gas-price 300000 --etherscan-api-key $BLASTSCAN_TOKEN
```
#### Verify on Mainnet
## Envelop SaftV2 Set in Blast Mainnet 2024-03-20 
**BatchWorkerV2**  
  https://blastscan.io/address/0x3F4deAC77d3aEb6aF2B827Cf8B0e772239F2D3e9#code

  
**TrustedWrapperV2** 
  https://blastscan.io/address/0x44C1bCE3f284e9a672fa4ba6f5b02E8651da88a0#code

**Tech Token**
  https://blastscan.io/address/0xF086ed4652A6d80CBEbB5A6d6Bb26A3C239c0ecC


```shell
forge verify-contract ...  ./contracts/BatchWorkerV2.sol:BatchWorkerV2 --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21  --constructor-args $(cast abi-encode "constructor(address param1)" 0x68247DF83d594af6332bF901a5fF8c3448622774)
```
```shell
forge verify-contract ...  ./contracts/BatchWorkerV2.sol:BatchWorkerV2 --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21  --constructor-args $(cast abi-encode "constructor(address param1)" 0x68247DF83d594af6332bF901a5fF8c3448622774)
```

```shell
forge verify-contract ...  ./contracts/TrustedWrapperV2.sol:TrustedWrapperV2 --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21  --constructor-args $(cast abi-encode "constructor(address,address)" 0xF086ed4652A6d80CBEbB5A6d6Bb26A3C239c0ecC 0x3F4deAC77d3aEb6aF2B827Cf8B0e772239F2D3e9)
```

```shell
forge verify-contract ...  lib/envelop-protocol-v1/contracts/TechTokenV1.sol:TechTokenV1 --verifier-url 'https://api.blastscan.io/api' --etherscan-api-key $BLASTSCAN_TOKEN --num-of-optimizations 200 --compiler-version 0.8.21  
```


```shell
$ forge script script/Counter.s.sol:CounterScript --rpc-url <your_rpc_url> --private-key <your_private_key>
```

### Cast

```shell
$ cast <subcommand>
```

### Help

```shell
$ forge --help
$ anvil --help
$ cast --help
```
### Add forge to existing Brownie project
```shell
$ forge init --force
$ forge install OpenZeppelin/openzeppelin-contracts@v4.9.3
$ forge install dao-envelop/envelop-protocol-v1@1.3.0
$ forge install dao-envelop/subscription@2.2.0
$ forge buld
```